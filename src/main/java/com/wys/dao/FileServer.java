package com.wys.dao;

import com.wys.observer.Observable;
import com.wys.observer.Observer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import static com.wys.FileServerObserver.executorService;

/*
 * @author wys
 * @version 1.0.0
 * @class FileServer$
 * @description
 * @aate 2023//7$  15:35$
 */
public class FileServer implements Observable {
    /*
     * 拿到当前服务端
     */
    private ServerSocket server;
    /*
     * 拿到观察者,即客户端的
     */
    private List<Observer> observers = new ArrayList<>();

    /*
     * 构造方法  创建服务端
     * @param port
     * @throws IOException
     */
    public FileServer(int port) throws IOException {
        server = new ServerSocket(port);
    }

    /*
     * 添加观察者
     * @param observer
     */
    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    /*
     * 移除观察者
     * @param observer
     */
    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    /*
     * 通知观察者,有变化则通知
     * @param message
     * @param socket
     */
    @Override
    public void notifyObservers(String message, Socket socket) {
        for (Observer observer : observers) {
            observer.update(message,socket);
        }
    }

    /*
     * 启动服务端
     * @throws IOException
     */
    public void start() throws IOException {
        System.out.println("===========File服务端启动================");

        while (true) {
            Socket socket = server.accept();
            System.out.println("有客户端连接");

            InputStream inputStream = socket.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            System.out.println("等待客户端发送id消息...");
            String id;
            while ((id = bufferedReader.readLine()) != null) {
                break;
            }
            if("exit".equals(id)){
                System.out.println("服务端退出");
                executorService.shutdown();//关闭线程池
                break;
            }
            System.out.println("客户端的id为：" + id);
            notifyObservers(id, socket);
        }
    }
}
