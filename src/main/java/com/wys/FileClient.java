package com.wys;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import com.wys.exception.FileClientException;
import com.wys.utils.FileUtil;

import static com.wys.utils.FileUtil.NEW_LINE;

/*
 * @Author wys
 * @Date 2024/1/5
 * @Description File客户端
 */
public class FileClient {
    /*
     * 发送消息结束标志
     */
    private static final String END_OF_MESSAGE = "END_OF_MESSAGE";
    /*
     * 客户端ID
     */
    private static String userId = "2";
    public static void main(String[] args) {

        System.out.println("===========File客户端启动================");

        //创建socket并根据IP地址与端口连接服务端
        Socket socket = null;
        try {
            socket = new Socket("127.0.0.1", 8888);
            new FileClient.FileClientOutputThread(socket).start();
            new FileClient.FileClientInputThread(socket).start();
        } catch (Exception e) {
            FileUtil.logError(e);
        }
    }

    /*
     * @description: 客户端的输入线程类
     * @author: wys
     * @date: 2024/01/08  20/03
     */
    static class FileClientInputThread extends Thread {
        private final Socket socket;
        public FileClientInputThread(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try (OutputStream outputStream = socket.getOutputStream();
                 InputStream inputStream = socket.getInputStream()) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                while (true) {
                    List<String> message = readMessage(bufferedReader);
                    if (message.isEmpty()) {
                        continue;
                    }

                    try {
                        handleClientMessage(outputStream, message);
                    } catch (Exception e) {
                        FileUtil.logError(e);
                        throw new FileClientException("处理客户端消息时发生异常", e);
                    }
                }
            } catch (Exception e) {
                throw new FileClientException("",e);
            }
        }
    }

    /*
     * @description: 客户端的输出线程类
     * @author: wys
     * @date: 2024/01/08  20/03
     */
    static class FileClientOutputThread extends Thread {

        private final Socket socket;
        public FileClientOutputThread(Socket socket){
            this.socket = socket;
        }
        @Override
        public void run() {
            try (                OutputStream outputStream = socket.getOutputStream();
                                 Scanner scanner = new Scanner(System.in);){
                //向服务端发送id
                outputStream.write((userId+"\n").getBytes());
                outputStream.flush();
                System.out.println("=====请输入文件路径或者输入exit退出登录：======");
                while (true) {
                    if(scanner.hasNext()) {
                        String filePath = scanner.nextLine();
                        writeMessage(outputStream, filePath);
                    }
                }
            } catch (Exception e) {
                throw new FileClientException("",e);
            }
        }
    }

    /*
     * @description: 处理客户端消息 如果是文件发送，则调用FileUtil发送文件，如果是打印消息，则调用FileUtil打印消息
     * @param outputStream
     * @param message
     * @throws Exception
     */
    private static void handleClientMessage(OutputStream outputStream, List<String> message) throws Exception {
        if (message.size() == 1) {
            // Handle file sending
            String dataString = message.get(0);
            byte[] data = FileUtil.readFileToByteArray(dataString);
            sendOutput(outputStream, data);
        } else if (message.size() == 2) {
            // Handle message print
            String clientName = message.get(0);
            String messageContent = FileUtil.decode(message.get(1));
            printMessage(clientName, messageContent);
        }
    }
    /*
     * @description: 发送文件
     * @param outputStream
     * @param data
     * @throws IOException
     */
    private static void sendOutput(OutputStream outputStream, byte[] data) throws IOException {
        outputStream.write(data);
        outputStream.write(NEW_LINE.getBytes());
        outputStream.write(END_OF_MESSAGE.getBytes());
        outputStream.write(NEW_LINE.getBytes());
        outputStream.flush();
        System.out.println("根据请求，成功发送文件给服务器");
    }

    /*
     * @description: 打印消息
     * @param clientName
     * @param messageContent
     */
    private static void printMessage(String clientName, String messageContent) {
        System.out.println("客户端" + clientName + "发送给你的消息为:");
        System.out.println(messageContent);
    }

    /*
     * @description: 向服务端发送消息，如果是文件发送，则调用FileUtil发送文件，如果是向指定客户端id发送消息，则多发送id给服务端
     * @param outputStream
     * @param filePath
     * @throws Exception
     */
    private static void writeMessage(OutputStream outputStream, String filePath) throws Exception {
        if ("exit".equals(filePath)) {
            throw new FileClientException("我退出登录了");
        }
        String[] msg = filePath.split(" ");
        if(msg.length==1) {
            System.out.println("开始发送文件");
            byte[] data = FileUtil.readFileToByteArray(filePath);
            outputStream.write(data);
            outputStream.write("\n".getBytes());
            outputStream.flush();
            outputStream.write(END_OF_MESSAGE.getBytes());
            outputStream.write("\n".getBytes());
            outputStream.flush();
            System.out.println("发送文件成功");
        }
        if(msg.length==3){
            System.out.println("开始发送文件");
            outputStream.write(msg[1].getBytes());
            outputStream.write("\n".getBytes());
            outputStream.flush();
            byte[] data = FileUtil.readFileToByteArray(msg[2]);
            outputStream.write(data);
            outputStream.write("\n".getBytes());
            outputStream.flush();
            outputStream.write(END_OF_MESSAGE.getBytes());
            outputStream.write("\n".getBytes());
            outputStream.flush();
            System.out.println("发送文件成功");
        }
    }

    /*
     * @description: 从服务端读取消息
     * @param bufferedReader
     * @return
     * @throws IOException
     */
    public static List<String> readMessage(BufferedReader bufferedReader) throws IOException {
        List<String> message = new ArrayList<>();
        String msg;
        while ((msg = bufferedReader.readLine()) != null) {
            if (msg.equals(END_OF_MESSAGE)) {
                break;
            }
            message.add(msg);
        }
        return message;
    }
}