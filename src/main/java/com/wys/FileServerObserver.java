package com.wys;

import com.wys.dao.FileServer;
import com.wys.observer.Observer;
import com.wys.thread.FileServerThread;

import java.io.IOException;
import java.net.Socket;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.*;

/*
 * @author wys
 * @version 1.0.0
 * @class FileServerObserver$
 * @description 文件服务器观察者
 * @aate 2024/1/7$  16:35$
 */
public class FileServerObserver implements Observer {
    /*
     * 存储用户的id信息,用于判断用户是否在线
     */
    public static Vector<String> userIdVector = new Vector<>();
    /*
     * 存储用户id和socket的映射关系，用于通过id找到用户的socket，可以可指定用户通信
     */
    public static Map<String,Socket> socketMap = new ConcurrentHashMap<>();

    /*
     * 线程池大小
     */
    private static final int THREAD_POOL_SIZE = 10;
    //创建线程池
    public static final ExecutorService executorService = new ThreadPoolExecutor(
            THREAD_POOL_SIZE,
            THREAD_POOL_SIZE,
            0L,
            TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>());

    /*
     * @param id  用户的id
     * @param socket 用户的socket
     */
    @Override
    public void update(String id, Socket socket) {
        if (userIdVector.contains(id)) {
            System.out.println("客户端" + id + "已上线");
            // 处理现有用户逻辑
        } else {
            System.out.println("新客户端" + id + "已上线");
            userIdVector.add(id);
            socketMap.put(id, socket);
            // 处理新用户逻辑 启动线程
            executorService.submit(new FileServerThread.FileServerInputTask(id,socket));
            executorService.submit(new FileServerThread.FileServerOutputTask(id,socket));
        }
    }

    public static void main(String[] args) throws IOException {
        FileServer fileServer = new FileServer(8888);
        FileServerObserver fileServerObserver = new FileServerObserver();

        fileServer.addObserver(fileServerObserver);

        fileServer.start();
    }
}

